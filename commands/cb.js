const boxen = require('boxen');
const cleverbot = require("cleverbot.io")
const clever = new cleverbot('MzNwGnyfgL1iW54C','uZBfESqRedUjuajf0DJ78jD5LWEK5JWe');

exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars

var clevermessage = message.content.split(' ').slice(1).join(' ')

if(clevermessage.length < 1) return message.channel.send("Please provide a message.")

clever.create(function (err, session) {
  clever.ask(clevermessage, function (err, response) {
    message.channel.send(message.author.toString() + ', ' + response);
    message.channel.stopTyping()
    console.log(boxen('[Cleverbot] ' + message.guild.name + ' | ' + message.author.tag + ' | ' + clevermessage + ' | ' + response, {padding: 1}))
	const { Client, RichEmbed } = require('discord.js');
    const cbEmbed = new RichEmbed()
    .setColor(colors.system)
    .setTitle('CleverBot Command Used')
    .setDescription('**Message:** ' + clevermessage + '\n **Reply:** ' + response)
    .setAuthor(message.author.username, message.author.displayAvatarURL)
    if(testing) return testing.send({embed: cbEmbed})
	message.channel.send({cbEmbed});
  });
});
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "cb",
  category: "Sato Stuff",
  description: "CleverBot.io",
  usage: "cb [input]"
};